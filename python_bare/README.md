# Simple Python-based template for a reproducible computational project

## Scope

The content of this folder is a template for doing reproducible research with Python.

The template is applied to the study of the spectrum of a random matrix and the files used to study this problem are also included.

This template is based on Python and bash scripts, to be executed by terminal. 

## Description

The template can be run in its completeness with the command `./make all` or it can be run step by step as explained below.

### Step 1: Creating a reference folder

A folder containing reference data and plots can be created with `./make reference`. A copy of the data and plots obtained by the reaserch is saved and can be used 
for comparing results obtained with following runs.

### Step 2: Creating virtual setup

With the command `./make setup` a virtual environment is created and all the needed dependencies are installed. 

Dependencies:
- scipy (1.7.3)
- jupyter (1.0.0)
- matplotlib (3.5.1)

### Step 3: Analysis

The command `./make analysis` reproduces the analysis done in the reaserch. The inputs needed for the reference run are included in the folder inputs/. 
Other inputs can also be included as text files in this folder.

### Step 4: Compare data with reference run

The reference data can be used for comparison. By running `./make check` the eigenvalues, saved in ascending order, are compared in order to check if there are any 
discrepancies. 

### Step 5: Plots

Plots can be reproduced by running `./make plots`.

## Authors:
- Daniele Coslovich
- Karim Chaine
