#!/usr/bin/env python
# Production

# Compute eigenvalues and eigenvectors of a random hermitian matrix. The seed used in the original research is included in the file =data_production_input.txt=

import numpy
import os
from scipy.linalg import eig


def symmetric_random_matrix(N, delta):
    """Explicit method"""
    f = open('input/seed.txt')
    seed = int(f.read())
    numpy.random.seed(seed)
    matrix = numpy.random.random((N, N))
    matrix -= 0.5
    matrix *= 2 * delta
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if j >= i:
                matrix[j, i] = matrix[i, j]
    return matrix
    

matrix = symmetric_random_matrix(N=100, delta=1.0)
eigenvalues, eigenvectors = eig(matrix)
eigenvalues = numpy.real(eigenvalues)
sorted_eigenvalues = sorted(eigenvalues)

# Create the data/ folder
current_directory = os.getcwd()
final_directory = os.path.join(current_directory, 'data')
if not os.path.exists(final_directory):
   os.makedirs(final_directory)
   
numpy.savetxt('data/eigenvalues.txt', sorted_eigenvalues,
              header='columns: eigenvalues', delimiter='\n')
