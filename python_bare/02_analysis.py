#!/usr/bin/env python
# Analysis

# Python script to generate the histogram of eigenvalues. Plots go into =plots=.

import os
import numpy
import matplotlib
import matplotlib.pyplot as plt

# Read back the eigenvalues
eigenvalues = numpy.loadtxt('data/eigenvalues.txt', unpack=True)

# Build the histogram explictly
hist = numpy.histogram(eigenvalues)

# Create the plots/ folder 

current_directory = os.getcwd()
final_directory = os.path.join(current_directory, 'plots')
if not os.path.exists(final_directory):
   os.makedirs(final_directory)

# Let matplotlib do the job
plt.hist(eigenvalues, bins='auto')
plt.savefig('plots/spectrum.png')
# plt.show()
