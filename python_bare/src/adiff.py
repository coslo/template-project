#!/usr/bin/env python


# Approximate, math-aware diff between two files

import sys
import os
import re
import numpy

def main(f_1, f_2, rtol=1e-6, comments=False):
    
    with open(f_1) as fh_1, \
         open(f_2) as fh_2:

        for line_1, line_2 in zip(fh_1, fh_2):
            line_1 = line_1.strip()
            line_2 = line_2.strip()

            if len(line_1) == 0 and len(line_2) == 0:
                # Skip empty lines
                match = True

            elif len(line_1) > 0 and len(line_2) == 0 or len(line_1) == 0 and len(line_2) > 0:
                # One line is empty the other not, fail
                match = False

            elif line_1.startswith('#'):
                match = True
                if comments:
                    if re.match('.*[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].*', line_1):
                        match = True
                    else:
                        # Comment lines must match exactly
                        match = line_1 == line_2

            elif line_1.count('=') == 1:
                # text lines with a single equation can match approximately
                _, x = line_1.split('=')
                _, y = line_2.split('=')
                try:
                    match = True
                    numpy.testing.assert_allclose(x, y, rtol=rtol)
                except AssertionError:
                    match = False
                    
            elif not (line_1[0].isdigit() or line_1[0] == '+' or line_1[0] == '-'):
                # But all other text lines must match exactly
                # (lines that do not start with digit or +-)
                match = line_1 == line_2

            else:
                # Finally, columnar data of floats / int match approximately
                x = numpy.array([float(_) for _ in line_1.split()])
                y = numpy.array([float(_) for _ in line_2.split()])
                try:
                    match = True
                    numpy.testing.assert_allclose(x, y, rtol=rtol)
                except AssertionError:
                    match = False

            if not match:
                print('files {} and {} differ'.format(f_1, f_2))
                print('< {}:'.format(line_1))
                print('> {}:'.format(line_2))
                sys.exit(1)
        
if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('-c', '--comments', action='store_true', dest='comments', help='check comments')
    parser.add_argument('-r', '--rtol', action='store', dest='rtol', default=1e-6, type=float, help='relative tolerance')
    parser.add_argument(dest='files', nargs=2, help='files')
    args = parser.parse_args()

    main(args.files[0], args.files[1], rtol=args.rtol, comments=args.comments)
