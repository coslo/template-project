#!/bin/bash
# Helpers                                                          :noexport:

# Bash logger

start() {
    start=$(date +%s.%N)
}
stop() {
    [[ ! -x $(command -v bc) ]] && return
    [[ ! -z $@ ]] && msg="$@" || msg="done"
    if [[ ! -z $start ]] ; then
	duration=$(echo $(date +%s.%N) - $start | bc)
	interval=$(printf "%.1f" $duration)
	echo "...$msg in $interval seconds"
    fi
}
